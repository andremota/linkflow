/* global describe, it */

(function () {
  'use strict';

  var assert = require("assert")

  describe('Resolver tests', function () {
    describe('When resolving urls', function () {
      it('should return 1 found and 1 not found', function () {
        assert.equal(-1, [1,2,3].indexOf(5));
      });
    });
  });
})();
