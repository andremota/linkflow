
angular.module('linkflow')
  .directive('optionsForm', ['store', function(store) {

    return {
      templateUrl: '/templates/options-form.html',
      restrict: 'E',
      scope: {},
      link: function (scope, element, attr) {

        scope.options = {
          url: []
        };

        scope.add = function() {
          var url = scope.options.url;
          if(url.indexOf(scope.newUrl) >= 0){
            //allready added
            return;
          }
          scope.options.url.push(scope.newUrl);
          scope.newUrl = '';
        };

        scope.remove = function(elem) {
          var index = scope.options.url.indexOf(elem);
          scope.options.url.splice(index, 1);
        };

        scope.save = function() {
          store.save(scope.options, function() {

          });
        };

        store.get(function(data) {
          scope.options = data;
          scope.$apply();
        });

      }
    };

  }]);
